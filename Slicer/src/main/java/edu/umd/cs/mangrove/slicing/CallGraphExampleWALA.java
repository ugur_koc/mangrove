package edu.umd.cs.mangrove.slicing;

import java.io.IOException;
import java.util.Iterator;

import com.ibm.wala.core.tests.callGraph.CallGraphTestUtil;
import com.ibm.wala.ipa.callgraph.AnalysisCacheImpl;
import com.ibm.wala.ipa.callgraph.AnalysisOptions;
import com.ibm.wala.ipa.callgraph.AnalysisScope;
import com.ibm.wala.ipa.callgraph.CGNode;
import com.ibm.wala.ipa.callgraph.CallGraph;
import com.ibm.wala.ipa.callgraph.CallGraphBuilder;
import com.ibm.wala.ipa.callgraph.CallGraphBuilderCancelException;
import com.ibm.wala.ipa.callgraph.Entrypoint;
import com.ibm.wala.ipa.callgraph.impl.DefaultEntrypoint;
import com.ibm.wala.ipa.callgraph.impl.Util;
import com.ibm.wala.ipa.callgraph.propagation.InstanceKey;
import com.ibm.wala.ipa.cha.ClassHierarchy;
import com.ibm.wala.ipa.cha.ClassHierarchyException;
import com.ibm.wala.ipa.cha.ClassHierarchyFactory;
import com.ibm.wala.shrikeCT.InvalidClassFileException;
import com.ibm.wala.types.Descriptor;
import com.ibm.wala.types.MethodReference;
import com.ibm.wala.types.TypeName;
import com.ibm.wala.types.TypeReference;
import com.ibm.wala.util.CancelException;
import com.ibm.wala.util.WalaException;
import com.ibm.wala.util.strings.Atom;

public class CallGraphExampleWALA {

	public static void main(String[] args)
			throws IOException, IllegalArgumentException, CancelException, WalaException, InvalidClassFileException {
		runRhino();
		//runAllocationsExample();
	}

	private static void runRhino() throws IOException, ClassHierarchyException, CallGraphBuilderCancelException {
		String exclusionsFile = "scopeFiles/Java60RegressionExclusions.txt";
		String scopeFile = "scopeFiles/rhino_scope.txt";
		String className = "Lorg/mozilla/javascript/tools/shell/Main";
		String funcName = "main";
		String funcDesc = "([Ljava/lang/String;)V";
		CallGraph cg = computeCallGraph(exclusionsFile, scopeFile, className, funcName, funcDesc);
		printCG(cg);
	}

	static void runAllocationsExample() throws IOException, ClassHierarchyException, CallGraphBuilderCancelException {
		String exclusionsFile = "scopeFiles/Java60RegressionExclusions.txt";
		String scopeFile = "scopeFiles/allocations_scope.txt";
		String className = "Lexamples/Allocations";
		String funcName = "main";
		String funcDesc = "([Ljava/lang/String;)V";
		CallGraph cg = computeCallGraph(exclusionsFile, scopeFile, className, funcName, funcDesc);
		printCG(cg);
	}

	private static CallGraph computeCallGraph(String exclusionsFile, String scopeFile, String className, String funcName,
			String funcDesc) throws IOException, ClassHierarchyException, CallGraphBuilderCancelException {
		AnalysisScope scope = CallGraphTestUtil.makeJ2SEAnalysisScope(scopeFile, exclusionsFile);
		ClassHierarchy cha = ClassHierarchyFactory.make(scope);
		Iterable<Entrypoint> entrypoints = computeEntryPoints(className, funcName, funcDesc, scope, cha);
		AnalysisOptions opts = CallGraphTestUtil.makeAnalysisOptions(scope, entrypoints);
		CallGraphBuilder<InstanceKey> builder = Util.makeZeroOneCFABuilder(opts, new AnalysisCacheImpl(), cha, scope);
		return builder.makeCallGraph(opts, null);
	}

	private static void printCG(CallGraph cg) {
		for (CGNode cgNode : cg) {
			TypeReference clazz = cgNode.getMethod().getReference().getDeclaringClass();
			if (!clazz.getClassLoader().toString().contains("Primordial")) {
				if (cgNode.getMethod().isStatic()) System.out.print("static ");
				System.out.println(cgNode.getMethod().getName() + " " + clazz);
			}
		}
	}

	private static Iterable<Entrypoint> computeEntryPoints(final String fileName, final String funcName,
			final String funcDesc, final AnalysisScope scope, final ClassHierarchy cha) {
		return new Iterable<Entrypoint>() {
			public Iterator<Entrypoint> iterator() {
				final Atom mainMethod = Atom.findOrCreateAsciiAtom(funcName);
				return new Iterator<Entrypoint>() {
					private int index = 0;

					public boolean hasNext() {
						return index < 1;
					}

					public Entrypoint next() {
						index++;
						TypeReference T = TypeReference.findOrCreate(scope.getApplicationLoader(),
								TypeName.string2TypeName(fileName));
						MethodReference mainRef = MethodReference.findOrCreate(T, mainMethod,
								Descriptor.findOrCreateUTF8(funcDesc));

						return new DefaultEntrypoint(mainRef, cha);
					}
				};
			}
		};
	}
}